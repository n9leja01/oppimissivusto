$(document).ready(function(){
   let range = 0;
   let type = 0;
   let quantity = 0;
   let types = [" + ", " - ", " x "];
   let tulos = 0;
   let laskuri = 0;
   let oikeinlasketut = 0;
    
    // funktiolla arvotaan random-arvoja
    function getRndInteger(min, max) { 
    return Math.floor(Math.random() * (max - min + 1)) + min;
    } 
    
    // tehdään funktio, joka laskee tuloksen
    function getResult(num1, num2, type) { 
      if (type === 0)
          return num1 + num2;
      else if (type === 1)
          return num1 - num2;
      else
          return num1 * num2;
    }
    
    // funktio luotu, matikkatehtävä toteuttaa samaa "kaavaa" yhä uudestaan.
    function teeHelahoito (type, range) { 
    
        let tyyppi = getRndInteger(0, type); // arvotaan laskun tyyppi 
        let num1 =  getRndInteger(1, range); // ensimmäinen arvo
        let num2 =  getRndInteger(1, range); // toinen arvo     
    
        // Nyt lasketaan lasketut laskut:
        laskuri++;
            
        $("#lauseke").html(num1 + types[tyyppi] + num2 + " =");
        
        tulos = getResult(num1, num2, tyyppi); // käytetään luotua tulos-funktiota 
        
        $("#checkresult").prop('disabled', false); // kun klikattu niin "tarkista" -nappi käytössä
        }
    
    $("#start").click(function () {
        // luetaan radio buttonit
        range = $("input[name=num_scale]:checked").val(); //täällä valinta lukualueista
        type = $("input[name=type]:checked").val(); // täällä valita laskutyypeistä
        quantity = $("input[name=quantity]:checked").val(); // täällä valinta laskujen määrästä  

        $("#vastaus").removeClass("not_visible"); // vastaus -kenttä piilossa kunnes aloitetaan
        $("#checkresult").removeClass("not_visible"); // Tarkista vastaus -kenttä piilossa kunnes aloitetaan
        $(this).addClass("not_visible");
        
        teeHelahoito (type, range); // suorittaa funktion
        
        $("#vastaus").focus(); // fokus kenttään
               
    });
       // tarkistetaan vastaus 
    $("#checkresult").click(function (){ 
        let vastaus = Number($("#vastaus").val());
         
       if (vastaus === tulos){
           $("#oikeavastaus").html("OIKEIN!&nbsp<i class='far fa-grin-stars'></i>");
           oikeinlasketut++;
           
       } else {
           $("#oikeavastaus").html("Voi harmi. Oikea vastaus on: " + tulos);
       }
        $("#vastatut").html(laskuri + " / " + quantity);
        $("#oikeinlasketut").html(oikeinlasketut + " / " + quantity);
        
        if (laskuri < quantity)  
          $("#next").removeClass("not_visible"); // Seuraava -nappi toimii niin kauan kuin tehtäviä riittää
       else {
           $("#palkinto").removeClass("not_visible");
           if (oikeinlasketut >= 0.75 * quantity || oikeinlasketut >= quantity - 1)
               $("#lopputulos").html("LOPPUTULOS: <br><br> <i class='fas fa-award'></i> &nbsp WAU, TAIDAT OLLA <br> OIKEA MATIKKANERO! &nbsp <i class='far fa-grin-stars'></i>");
           else
               $("#lopputulos").html("HYVÄ YRITYS,<br> TREENAATHAN VIELÄ <br> TULLAKSESI MESTARIKSI.");
       }
        $(this).prop('disabled', true); // kun klikattu niin disabloidaan "tarkista" -nappi käytöstä
    });    
    // seuraava napin funktio
    $("#next").click(function(){
        $("#oikeavastaus").html(""); // tyhjennetään aiempi oikea vastaus
        $("#vastaus").val(""); // tyhjennetään käyttäjän vastaus
        $("#vastaus").focus(); // fokus kenttään
        $(this).addClass("not_visible");
      
       teeHelahoito (type, range); // taas uudestaan 
   });
});


