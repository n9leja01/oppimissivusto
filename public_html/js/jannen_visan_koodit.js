/*
 * Author: Janne Lehmikangas
 */

$(document).ready(function () {
    let i = 0;
    let o = 0;

    $(".choice").click(function () {
        i++;
        let attribuutinArvo = $(this).attr("name");
        // lukitaan valinta
        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        // tarkistetaan vastaus
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            // oikea vastaus
            o++;
            $(this).parent().addClass("selected").addClass("right");
            let attribuutinArvo = $(this).attr("name");
            // paljastetaan lisätieto
            $("#" + attribuutinArvo).removeClass("not_visible");
        } else {
            // väärä vastaus
            $(this).parent().addClass("wrong");
            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "][value=1]";
            // merkitään oikea vastaus
            $(valinta).parent().addClass("marked");
        }

        let prosenttia = Number(o) / Number(i) * 100;
        $(".vastatut").html(" " + i + " ");
        $(".oikeat").html("Oikeat vastaukset: " + o + " / " + i + " ("
                + prosenttia.toFixed(0) + " %)");

        // näytä modali, jos kaikkiin kysymyksiin on vastattu
        if (i >= 8) {
            $("#valmisModal").modal("show");
            if (prosenttia >= 50) {
                $("#tulos").addClass("right");
                $("#valmisModalLabel").html("Hyvä!");
            } else {
                $("#tulos").addClass("wrong");
                $("#valmisModalLabel").html("Vielä on parannettavaa");
            }
        } 
    });
});
