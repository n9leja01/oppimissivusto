/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
   let lukualue = 0;
   let laskut = 0;
   let summa = 0;
   let laskuri = 0;
   let oikeinlasketut = 0;
   
   
   
   function getRndInteger(min, max) { // Random arvot
    return Math.floor(Math.random() * (max - min + 1)) + min;
   }
   
   //pluslasku
   function laskeSumma(num1, num2) {
      return num1 + num2;
   }
   
   //arvotaan numerot käyttäjälle ja määritellään oikea vastaus jota haetaan
   function teeLasku(lukualue){
       let num1 = getRndInteger(1, lukualue);
       let num2 = getRndInteger(1, lukualue);
       
       
       $("#lauseke").html(num1 + " + " + num2 + " =");
       
        summa = laskeSumma(num1, num2);
        
        
       $("#tarkista").prop('disabled', false);
   }
   
   $("#aloita1").click(function () {
        // luetaan radio buttonit
        lukualue = $("input[name=num_scale]:checked").val(); //lukualuevalinta
        laskut = $("input[name=laskut]:checked").val(); // laskujen määrä  

        $("#vastaus").removeClass("not_visible"); // vastauskenttä tulee näkyviin nappia painamalla
        $("#tarkista").removeClass("not_visible"); // tarkista nappi tulee näkyviin
        $(this).addClass("not_visible");
        
        teeLasku (lukualue); // suorittaa funktion
        
        $("#vastaus").focus(); // fokus kenttään
        $(".piilota").hide();      
    });
    
    $("#tarkista").click(function (){ // tarkistetaan vastaus
        let vastaus = Number($("#vastaus").val());
         laskuri++;
       if (vastaus === summa){
           $("#oikeavastaus").html('<i class="far fa-smile-wink fa-3x"></i><br>Oikein!');
           oikeinlasketut++;
           
       } else {
           $("#oikeavastaus").html('<i class="fas fa-dizzy"></i> Väärin. Oikea vastaus on: ' + summa);
       }
       //näyttää lasketut ja oikeinlasketut laskut
        $("#laskuri").html(laskuri + " / " + laskut);
        $("#oikeinlasketut").html(oikeinlasketut + " / " + laskut);
        
        if (laskuri < laskut)  {    
          $("#seuraava").removeClass("not_visible"); // Seuraava -nappi toimii niin kauan kuin tehtäviä riittää
        } else {
            $("#nappi").removeClass("not_visible"); // näyttää modal-buttonin kun laskut loppuvat
        }
        $(this).prop('disabled', true); // kun klikattu niin disabloidaan "tarkista" -nappi käytöstä
    });    
    
    $("#seuraava").click(function(){
        $("#oikeavastaus").html(""); // tyhjennetään aiempi oikea vastaus
        $("#vastaus").val(""); // tyhjennetään käyttäjän vastaus
        $("#vastaus").focus(); // fokus kenttään
        $(this).addClass("not_visible");
      
       teeLasku (lukualue); // taas uudestaan 
       
       
   });

});
