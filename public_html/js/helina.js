/* Helinä Marttila
 * Habanerot
 * Web-projekti 2020 */
 

$(document).ready(function () {
    
    let vastatut = 0;

        $(".choice").click(function () {
            // mikä valintanappiryhmä?
            let attribuutinArvo = $(this).attr("name");

            // ei saa valita uudestaan
            let valinta = "[name=" + attribuutinArvo + "]";
            $(valinta).prop("disabled", true);

            // onko valinta oikein
            let vastaus = Number($(this).val());
            if (vastaus === 1) {
                // oikea vastaus
                $(this).parent().addClass("selected").addClass("oikea");
            } else {
                $(this).parent().addClass("väärä");
                // [name=xxx] [value=1]
                let attribuutinArvo = $(this).attr("name");
                let valinta = "[name" + attribuutinArvo + "] [value=1]";
                $(valinta).parent().addClass("oikea");
            }
            
           // näytetään selittävä teksti
           $("#" + attribuutinArvo).removeClass("not_visible");
           
           //lisätään vastausten määrä laskuriin
           vastatut = vastatut + 1;
           $("#vastatut").html(vastatut + " / 10");
        });

});


