/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    let visat = ['#visa1', '#visa2', '#visa3', '#visa4', '#visa5', '#visa6', '#visa7', '#visa8'];
    let näkyvissä = 0;
    let vastatut = 0;
    //ensimmäinen kysymys näkyville
    $("#aloita").click(function(){
    $(visat[näkyvissä]).removeClass("not_visible");
    $(this).addClass("not_visible");
    });
    
    $(".choice").click(function(){
        // mikä valintanappiryhmä?
      let attribuutinArvo = $(this).attr("name");
      
      // ei saa valita uudestaan
      let valinta = "[name=" + attribuutinArvo + "]";
      $(valinta).prop("disabled", true);
    
    vastatut++;
      $(".vastatut").html(" " + vastatut + " ");
      // onko valinta oikein
      let vastaus = Number($(this).val());
      if (vastaus === 1) {
          // oikea vastaus
          $(this).parent().addClass("selected").addClass("right");
      } else {
          $(this).parent().addClass("wrong");
          // [name=xxx] [value=1]
          let attribuutinArvo = $(this).attr["name"];
          let valinta = "[name" + attribuutinArvo + "] [value=1]";
          $(valinta).parent().addClass("right");
      }
      $("#" + attribuutinArvo).removeClass("not_visible");
      $("#jatka").removeClass("not_visible");
      });
      
       $("#jatka").click(function(){
      $(visat[näkyvissä]).addClass("not_visible");
      näkyvissä++;
      // vieläkö on näytettävää
      if (näkyvissä < visat.length) {
          $(visat[näkyvissä]).removeClass("not_visible");
             //modaalinappi näkyviin
      } else {
          $(this).addClass("not_visible");
          $("#nappi").removeClass("not_visible");
      }
      
      
   });
      
});


