/*
 * Author: Janne Lehmikangas
 */

$(document).ready(function () {
    let i = 0;
    let o = 0;
    let tulos = 0;
    let jakaja = 0;
    let jaettava = 0;
    let prosenttia = 0;
    /**
     * Generates a random number in a min - max range
     * 
     * @param {Number} min  minimum value for a random number
     * @param {Number} max  maximum value for a random number
     * @returns {Number}    generated random number
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function tulostaMäärät() {
        prosenttia = Number(o) / Number(i) * 100;
        $("#vastatut").html(" " + i + " ");
    }

    function muodostaTeht() {
        tulos = getRndInteger(1, 10);
        jakaja = getRndInteger(1, 10);
        jaettava = jakaja * tulos;

        $("#teht").html(jaettava + " / " + jakaja + " =");

        i++;
    }

    $("#aloita").click(function () {
        $("#tehtävä").removeClass("not_visible");
        $(this).parent(this).addClass("not_visible");

        muodostaTeht();
        tulostaMäärät();
    });

    $("#tarkista").click(function () {
        let vastaus = Number($("#vastaus").val());

        if (vastaus === tulos) {
            $("#oikeavastaus").html("<i class='fa fa-thumbs-o-up fa-2x' aria-hidden='true'></i>");
            o++;

        } else {
            $("#oikeavastaus").html("Väärin, oikeavastaus on: " + tulos);
        }

        tulostaMäärät();
        $(".oikeat").html(o + " / " + i + " ("
                + prosenttia.toFixed(0) + " %)");

        if (i < 8) {
            $("#seuraava").attr("disabled", false); // Seuraava -nappi toimii niin kauan kuin tehtäviä riittää
        } else {
            $("#valmisModal").modal("show");
            if (prosenttia >= 50) {
                $("#tulosModal").addClass("right");
                $("#valmisModalLabel").html("Hyvä!");
            } else {
                $("#tulosModal").addClass("wrong");
                $("#valmisModalLabel").html("Vielä on parannettavaa");
            }
        }
        $(this).attr("disabled", true);
    });

    $("#seuraava").click(function () {
        muodostaTeht();
        tulostaMäärät();
        $("#oikeavastaus").html("");
        $("#vastaus").val("");
        $("#vastaus").focus();
        $(this).attr("disabled", true);
        $("#tarkista").attr("disabled", false);
    });
});