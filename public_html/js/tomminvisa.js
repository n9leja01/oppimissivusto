$(document).ready(function () {

    $(".choice").click(function () {
        let attribuutinArvo = $(this).attr("name");
        // lukitaan valinta
        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        // tarkistetaan vastaus
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            // oikea vastaus
            $(this).parent().addClass("selected").addClass("right");
            let attribuutinArvo = $(this).attr("name");
            // paljastetaan lisätieto
            $("#" + attribuutinArvo).removeClass("not_visible");
        } else {
            // väärä vastaus
            $(this).parent().addClass("wrong");
            let attribuutinArvo = $(this).attr("name");
            let valinta = "[name=" + attribuutinArvo + "][value=1]";
            // merkitään oikea vastaus
            $(valinta).parent().addClass("marked");            
        }        
    });
});


