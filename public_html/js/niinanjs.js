$(document).ready(function () {
    let quizzes = ['#quiz1', '#quiz2', '#quiz3', '#quiz4', '#quiz5', '#quiz6', '#quiz7', '#quiz8']; //yhteensä 8 visatehtävää
    let visible = 0; 
    let vastatut = 0;

    // kun aloita-nappia painetaan niin 1. kysymys ilmestyy. 
    // Aloita-napin klikkaamisen jälkeen se poistuu.
    $("#start").click(function () {
        $(quizzes[visible]).removeClass("not_visible");
        $(this).addClass("not_visible");
    });

    // valinta   
    $(".choice").click(function () {
        
        let attribuutinArvo = $(this).attr("name");

        // ei saa valita uudestaan
        let valinta = "[name=" + attribuutinArvo + "]";
        $(valinta).prop("disabled", true);

        // laskuri laskee moneenko kysymykseen vastattu
        vastatut++;
        $(".vastatut").html(" " + vastatut + " ");

        // tutkii onko valinta oikein
        let vastaus = Number($(this).val());
        if (vastaus === 1) {
            
            $(this).parent().addClass("selected").addClass("right");
        } else {
            $(this).parent().addClass("wrong");
            // [name=xxx] [value=1]
            let attribuutinArvo = $(this).attr["name"];
            let valinta = "[name" + attribuutinArvo + "] [value=1]";
            $(valinta).parent().addClass("right");
        }

        $("#" + attribuutinArvo).removeClass("not_visible");
        if (vastatut < 8)
            $("#next").removeClass("not_visible");
    });
    // seuraava -nappi funktio
    $("#next").click(function () {
        $(quizzes[visible]).addClass("not_visible");
        visible++;
        // vieläkö on näytettävää
        if (visible < quizzes.length) {
            $(quizzes[visible]).removeClass("not_visible");

        } else {

        }
        $(this).addClass("not_visible");
    });
});


