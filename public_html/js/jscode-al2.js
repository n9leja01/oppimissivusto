/* 
* Aki Löthman
* Habanerot
* Web-projekti 2020 
*/

$(function(){
    let v = 0;
    let o = 0;
    let nro1 = 0;
    let nro2 = 0;
    let summa = 0;
    /**
     * Generates a random number in a min - max range
     * 
     * @param {Number} min  minimum value for a random number
     * @param {Number} max  maximum value for a random number
     * @returns {Number}    generated random number
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function Tulostus() {
        $("#vastatut").html(" " + v + " ");
    }

    function TeeTehtävä() {
      nro1 = getRndInteger(1, 25);
      nro2 = getRndInteger(1, 25);
      summa = nro2 + nro1;
      $("#tehtävä").html(summa + " - " + nro2 + " =");
        v++;
        // Numerot 50:een asti
    }

    $("#alotus").click(function () {
        $("#alotus-div").hide();
        $("#tehtäväloota").removeClass("not_visible"); 
        TeeTehtävä();
        Tulostus();
    });

    $("#tarkista").click(function () {
        let vastaus = Number($("#vastaus").val());
        if (vastaus === nro1) {
            $("#oikeavastaus").html("Oikein! Mahtavaa <i class='fa fa-thumbs-up fa' aria-hidden='true'></i>");
            $("#tähtiok").show();
            $("#tähdet").append("<i class='fa fa-star' aria-hidden='true'></i>");
            o++;
        } else {
            $("#oikeavastaus").html("<i class='fa fa-thumbs-down' aria-hidden='true'></i> Väärin, oikea vastaus on " + nro1);
        }
      
        Tulostus();
        if (v < 10) {
            $("#seuraava").attr("disabled", false); // nappi toimii niin kauan kuin tehtäviä riittää
        }
        $(this).attr("disabled", true);
        $("#uudestaan").attr("disabled", false);
    });
    
    $("#seuraava").click(function() {
        $("#oikeavastaus").html(""); //tyhjennys
        $("#vastaus").val(""); //tyhjennys inputista
        TeeTehtävä();
        Tulostus();
        $("#seuraava").attr("disabled", true);
        $("#tarkista").attr("disabled", false);
    });

    $("#uudestaan").click(function() {
        // location.reload(); // lataa koko sivuston uudestaan
        v = 0;
        o = 0;
        $("#tähtiok").hide();
        $("#tähdet").html(""); //tyhjennys
        $("#oikeavastaus").html(""); //tyhjennys
        $("#vastaus").val(""); //tyhjennys inputista
        TeeTehtävä();
        Tulostus();
        $("#uudestaan").attr("disabled", false);
        $("#tarkista").attr("disabled", false);
        $("#seuraava").attr("disabled", true);
    });

});