/* 
* Aki Löthman
* Habanerot
* Web-projekti 2020 
*/

oikeat = ["A","B","A","C","C","B","A","A"];
$(function(){
    $("label").click(function(){
        if(!$(this).hasClass("blocked")){
            vastaus=$(this).attr("for");
            $("label[for="+vastaus+"]").addClass("blocked");
            $("label[for="+vastaus+"] > input").attr("checked",false);
            $(this).children("input").attr("checked",true);
            if( $(this).children("input").attr("value")==oikeat[(vastaus.substr(1, 1))-1] ){
                $(this).addClass("green");
                $("#"+vastaus).show();
            }
            else{
                $(this).addClass("red");
            }
            $(this).children("input").attr("checked",true);
        }
    });
});