/* Helinä Marttila
 * Habanerot
 * Web-projekti 2020 */

$(document).ready(function () {

    let tehtLkm = 0;
    let maksimi = 0;
    let vastatut = 0;
    let oikeat = 0;

    let jakaja = 0;
    let osamäärä = 0;
    let jakojäännös = 0;
    let jaettava = 0;

    let tulos = [];


    /**
     * Generates a random number in a min - max range
     * 
     * @param {Number} min  minimum value for a random number
     * @param {Number} max  maximum value for a random number
     * @returns {Number}    generated random number
     */
    function getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }


    /**
     * Muodostaa laskulausekkeen ja tulostaa sen
     * Palauttaa taulukon, jonka arvoina osamäärä ja jakojäännös.
     */
    function teeLauseke() {
        jakaja = getRndInteger(1, maksimi);
        osamäärä = getRndInteger(1, 10);
        jakojäännös = getRndInteger(0, jakaja - 1);

        let jaettava = jakaja * osamäärä + jakojäännös;
        $("#jaettava").html(jaettava + " : ");
        $("#jakaja").html(jakaja + " = ");

        let arr = [osamäärä, jakojäännös];
        return arr;
    }


    /**
     * Tulostaa alert-ikkunan ruudulle.
     * Tyhjentää aikaisemmat valinnat ja tulokset
     */
    function teeLopetus() {
        alert("Hyvää työtä!");
        $("#vastatut").hide();
        $("#palaute").html("");
        $("#kukat").html("");
        $("#vastatut").html("");
        $("#jaettava").html("");
        $("#jakaja").html("");
    }


    $("#aloitetaan").click(function () {

        //Piilotetaan alkuvalinnat ja tyhjennetään aiempi yritys    
        $("#piilo").hide();
        $("#palaute").html("");
        $("#kukat").html("");
        $("#vastatut").addClass("not_visible");
        $("#jaettava").html("");
        $("#jakaja").html("");


        // Näytetään Tarkista- ja Seuraava-painikkeet
        $("#seuraava").removeClass("not_visible");
        $("#tarkista").removeClass("not_visible");

        // Luetaan käyttäjän valinnat: laskutehtävien lukumäärä ja maksimijakaja
        tehtLkm = $("input[name=lkm]:checked").val();
        maksimi = $("input[id=max]").val();


        // Esitetään laskutoimitus
        $("#vastaus").removeClass("not_visible");
        $("#vastaus").focus();
        $("#jäännös").removeClass("not_visible");

        tulos = teeLauseke();

    });


    $("#tarkista").click(function () {

        // luetaan käyttäjän vastaukset input-kentistä
        let vastaus = Number($("#vastaus").val());
        let jj = Number($("#jäännös").val());

        // verrataan käyttäjän vastausta osamäärän ja jakojäännöksen arvoihin
        if (vastaus === tulos[0] && jj === tulos[1]) {
            $("#palaute").html("Hienoa, vastasit oikein!");
            $("#kukat").append("<i class='fa fa-diamond' aria-hidden='true'></i>");
        } else {
            $("#palaute").html("Voi harmi. Oikea vastaus on " + tulos[0] +
                    ". Jakojäännös on " + tulos[1] + " .");
        }

        // näytetään laskuri ja vastattujen kysymysten määrä
        $("#vastatut").removeClass("not_visible");
        vastatut = vastatut + 1;
        $("#vastatut").html(vastatut + " tehtävää tehty, " +
                (tehtLkm - vastatut) + " tehtävää jäljellä");

        // tehtäviä vielä jäljellä? seuraava laskutehtävä tai "Valmis"      
        $("#tarkista").attr("disabled", true);
        if (vastatut < tehtLkm) {
            $("#seuraava").attr("disabled", false);
        } else {
            $("#vastaus").val("");
            $("#vastaus").focus();
            $("#jäännös").val("");
            $("#valmis").removeClass("not_visible");
        }
        
    });


    $("#seuraava").click(function () {

        // tyhjennetään edelliset vastaukset
        $("#vastaus").val("");
        $("#vastaus").focus();
        $("#jäännös").val("");

        tulos = teeLauseke();

        $("#tarkista").attr("disabled", false);
        $("#seuraava").attr("disabled", true);

    });
    

    // näyttää pokaalin kuvan, jonka click-funktio teeLopetus()
    $("#valmis").click(function () {
        $(this).hide();
        $("#pokaali").removeClass("not_visible");
    });

    $("#pokaali").click(function () {
        $(this).hide();
        teeLopetus();
    });


});

